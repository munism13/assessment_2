//load libraries
const express = require ("express");
const path = require ("path");
const mysql = require ("mysql");
const q = require("q");
const bodyParser = require ("body-parser");

const pool = mysql.createPool ({
    host:"mymachine", port: 3306,
    user:"as_charlie", password: "charlie",
    database:"grocery",
    connectionLimit: 4
});


const mkQuery = function (sql, pool) {
    return (function() {
		const defer = q.defer();
		//Collect arguments
		const args = [];
		for (var i in arguments)
			args.push(arguments[i]);

		pool.getConnection(function(err, conn) {
			if (err) {
				defer.reject(err);
                return;
                
			}

			conn.query(sql, args, function(err, result) {
				if (err) 
					defer.reject(err);
				else
					defer.resolve(result);
				conn.release();
			});
		});

		return (defer.promise);
	});
}

const SELECT_GROCERY_LIST_BY_BRAND =
"select * from GROCERY_LIST where BRAND = ?";

const SELECT_GROCERY_LIST_BY_NAME = 
"select * from GROCERY_LIST where NAME = ?";


const findGroceryListByBrand = 
mkQuery(SELECT_GROCERY_LIST_BY_BRAND, pool);

const findGroceryListByName = 
mkQuery(SELECT_GROCERY_LIST_BY_NAME, pool);

const app = express ();

app.use(bodyParser.urlencoded({extended: false }));
app.use(bodyParser.json());



app.get("/edit", function(req, resp) {
	
	const brands = parseInt(req.params.brand);

	findGroceryListByBrand(brands)
		.then(function(result) {
            resp.status(200);
            resp.type("application/json");
            resp.json(result);
            resp.send(JSON.stringify(result));
            console.log("successful search");

		}).catch(function(err) {
            resp.status(500);
            resp.type("application/json");
			resp.json(err);
		});
});




const handleError = function (err, result) {
    res.status(500);
    res.type("text/plain");
    res.send(JSON.stringify(err));
}



app.use(express.static(path.join(__dirname, "../client")));

app.use("/libs", express.static(path.join(__dirname, "../bower_components")));

const port = process.env.APP_PORT || 3000;
app.listen(port, function () {
    console.info("Database app started at %s on port %d", new Date(), port);
})