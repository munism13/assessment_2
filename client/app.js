(function () {


    var GroceryApp = angular.module("GroceryApp", []);



    var GroceryConfig = function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("main", {
                url: "/main",
                templateUrl: "/main/index.html",
                controller: "BrandCtrl as brandCtrl",
                controller: "GroceryCtrl as groceryCtrl"


            })
        .state("edit", {
            url: "/edit/:term",
            templateUrl: "/client/edit.html",
            controller: "EditCtrl as editCtrl"
        })

    }
    GroceryConfig.$inject = ["$stateProvider", "$urlRouterProvider"];


    var GrocerySvc = function ($http, $q) {
        var grocerySvc = this;

        grocerySvc.findBrands = function () {
            var defer = $q.defer();

            $http.get("/brands/" + brand)
                .then(function (result) {
                    defer.resolve(result.data.brand);

                }).catch(function (err) {
                    defer.reject(err);
                });

            return (defer.promise);
        }

        grocerySvc.findProductsByName = function () {
            var defer = $q.defer();

            $http.get("/products/" + Name)
                .then(function (result) {
                    defer.resolve(result.data.name);

                }).catch(function (err) {
                    defer.reject(err);
                });

            return (defer.promise);
        }
    };
    GrocerySvc.$inject = ["$http", "$q"];


    var GroceryCtrl = function () {
        var groceryCtrl = this;
    };


    var BrandCtrl = function ($state) {
        var brandCtrl = this;

        brandCtrl.brand = [];
        // brandCtrl.customer = {};

        brandCtrl.search = function () {
            console.info("Brands close to your search.. %s", brandCtrl.brand);
            $state.go("edit", { term: brandCtrl.brand });

            return (defer.promise);

        }

        BrandCtrl.$inject = ["GrocerySvc"]
    }

    var NameCtrl = function ($state) {
        var nameCtrl = this;

        nameCtrl.Name = [];

        nameCtrl.search = function () {
            console.log(">> Names that match ..%s", nameCtrl.Name);
            $state.go("edit", { term: nameCtrl.brand });


            return (defer.promise);
     
        }
        NameCtrl.$inject = ["GrocerySvc"]


        
    }

    // var app = angular.module("GroceryApp", []);

    GroceryApp.config(GroceryConfig);

    GroceryApp.service("GrocerySvc", GrocerySvc);

    GroceryApp.controller("BrandCtrl", BrandCtrl);
    GroceryApp.controller("GroceryCtrl", GroceryCtrl);



}()); 